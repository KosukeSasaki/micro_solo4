<?php

// --------------------
// 指定ディレクトリ内のディレクトリ数をカウント
// --------------------

function countDir($d)
{

    $files = scandir($d);
    $count = 0;
    foreach ($files as $file){
        if (is_dir($d . $file) == true){
            $count++;
        }
    }
    return $count-2;
}


// --------------------
// pictureディレクトリ内のディレクトリ数とファイル数の総数をカウント countPicdir();
// --------------------

function countPicdir($d)
{
    // $dir = scandir($d);    // pictureディレクトリを走査
    // return count($dir) - 3;   // ./ と ../ と .DS_Storeをカウント数から除外
    $dir = scandir($d);
    $count = 0;
    if ($handle = opendir($d)) {
        while (false !== ($entry = readdir($handle))) { 
            if(preg_match("/.png$/", $entry)){
                $count++;
            }
        }
    }
    return $count;
}


// --------------------
// 指定したディレクトリ番号を出力 outPass();
// --------------------

function outPass()
{
    $max_dir = countPicdir(PIC);
    $dir_num = mt_rand(1, $max_dir);
    return $dir_num;
}


// --------------------
// 指定したディレクトリからオリジナル画像を選択 choosePic();
// --------------------

function choosePic($dir_num)
{
    $pass = PIC.'/'.$dir_num.'/'.ORG;
    return $pass;
}


// --------------------
// 指定したディレクトリのファイル数をカウント countPic();
// --------------------

function countPic($d)
{
    $dir = scandir($d);
    $count = 0;
    if ($handle = opendir($d)) {
        while (false !== ($entry = readdir($handle))) { 
            if(preg_match("/.png$/", $entry)){
                $count++;
            }
        }
    }
    return $count;
}

?>
