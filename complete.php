<?php

session_start();
// require_once('db_init.php');	// データベースの初期情報を読み込む
require_once('const_init.php');   // 定数の定義
require_once('function.php');   // 関数定義ファイルの読み込み

$dir_num = $_SESSION['dir_num'];    // ディレクトリ番号
$dir_pass = PIC . '/' . $dir_num;   // 描画されたファイルのあるパス
$file_count = countPicDir($dir_pass);   // 描画ディレクトリのファイル数
$base_pass = $dir_pass . '/1.png';  // 描画ディレクトリの先頭ファイル

date_default_timezone_set('Asia/Tokyo');
$date = date(Y).date(m).date(d).date(H).date(i).date(s);
$file_pass = COMP . '/' . $dir_num . '_' . $date . '.png' ;   // 完成ファイルのフルパス
$_SESSION['comp'] = $file_pass;


$i = $_SESSION['iterator'];
$i++;
$_SESSION['iterator'] = $i;

// --------------------------
// imagecreatefrompng が動かない。ヤバい
// --------------------------
/*
if(file_exists($base_pass)){ // 描画ディレクトリの先頭ファイルがあれば完成ファイルの描画開始
    $base_img = imagecreatefrompng($base_pass);
    imagesavealpha($base_img, true);
    $sizex = imagesx($base_img);
    $sizey = imagesy($base_img);

    for($i = 1; $i <= $file_count; $i++){    // 先頭ファイルの次のファイルから合成開始
        $add_file = "$dir_pass" . '/' . $i . '.png';
        $add_img = imagecreatefrompng($add_file);
        imagesavealpha($add_img, true);
        imagecopy($base_img, $add_img, 0, 0, 0, 0, $sizex, $sizey);
    }
    //ブレンドモードを無効にする
    // imagealphablending($resize, false);
    header('Content-type: image/png');
    imagepng($base_img, $file_pass);
    imagedestroy($add_img);
    imagedestroy($base_img);
    imagedestroy($resize);
   
}
 */
$cntfile = "data/test/" . $dir_num . ".txt";
$skipfile = "data/skip/" . $dir_num . "_skip.txt";

// skip人数をカウント
$fp = fopen($cntfile, "r+");
$cnt = fgets($fp, 64);
// flock($fp, 2);
fseek($fp, 0);
$cnt = $cnt + 1;
fputs($fp, $cnt);
fclose($fp);

// skip場所をカウント
if(!file_exists($skipfile)) touch($skipfile);

$string = $file_count . ",";
file_put_contents($skipfile , $string, FILE_APPEND | LOCK_EX);

?>
