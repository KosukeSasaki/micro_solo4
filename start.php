<?php

session_start();
// require_once('db_init.php');	// データベースの初期情報を読み込む
require_once('const_init.php');   // 定数の定義
require_once('function.php');   // 関数定義ファイルの読み込み
$cond_array = $_SESSION['cond_array'];
$iterator = $_SESSION['iterator'];
$cond = $_SESSION['cond'];
$dir_num = $cond_array[$iterator];
$indDir = $_SESSION['indDir'];
$_SESSION['dir_num'] = $dir_num;
// $dsm = $_SESSION['displaySamePicture'];
// $fFlag = $_SESSION['firstFlag'];

if($iterator >= $cond) $includeFile = "finish.html";
else {
    switch($dir_num){
    case 1:
        $includeFile = "start1.html";
        break;
    case 2:
        $includeFile = "start2.html";
        break;
    case 3:
        $includeFile = "start3.html";
        break;
    case 4:
        $includeFile = "start4.html";
        break;
    }
}

$contents = "";
ob_start();
include($includeFile);
$contents = ob_get_contents();
ob_end_clean();
echo $contents;

?>

