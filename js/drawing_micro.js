jQuery.noConflict();
var j$ = jQuery;

compbreak = false;  // complete.jsで操作
fFlag = false;  // 制限時間によって終了 = true

j$(function() {
    var offset = 0;
    var startX;
    var startY;
    var flag = false;   // ボタンが押されている = true
    var canvas = j$('canvas').get(0);
    var comp = false;   // 完成した = true
    var sFlag = false;  // マウスがボタンを押しながら初めて動いた = true
    var actionTime = 0;
    var fintime = 7000; // 終了までの時間(ms)

    if (canvas.getContext) {
        var context = canvas.getContext('2d');
    }

    if(!fFlag) {
        setInterval(checkTime, 100);    // checkTime関数を100ms単位で呼び出し
    }

    function checkTime() {
        if(!sFlag) return;
        
        var nowTime = new Date().getTime();
        if(nowTime < actionTime + fintime) return;

        fFlag = true;
    }
 
    j$('canvas').mousedown(function(e) {
        flag = true;
        startX = e.pageX - j$(this).offset().left - offset;
        startY = e.pageY - j$(this).offset().top - offset;
        return false; // for chrome
    });

    j$('canvas').mousemove(function(e) {
        if(flag && !sFlag){
            actionTime = new Date().getTime();
            sFlag = true;
        }
                
        if (fFlag) {
            if(!comp){
                comp = true;
                d = canvas.toDataURL('image/png');
                d = d.replace('data:image/png;base64,', '');
                finishAlert();
            }
        } else {
            if (flag && !compbreak) {
                var endX = e.pageX - j$('canvas').offset().left - offset;
                var endY = e.pageY - j$('canvas').offset().top - offset;
                context.lineWidth = 2;
                context.beginPath();
                context.moveTo(startX, startY);
                context.lineTo(endX, endY);
                context.stroke();
                context.closePath();
                startX = endX;
                startY = endY;
            }
            if (sFlag){ // 描画を開始したら終了ボタンを消す
                target = document.getElementById("button");
                target.innerHTML = '';
            }
        }
    });
 
    j$('canvas').on('mouseup', function() {
        flag = false;
    });
 
    j$('canvas').on('mouseleave', function() {
        flag = false;
    });
 
    /*
    j$('li').click(function() {
        context.strokeStyle = j$(this).css('background-color');
    });
 
    j$('#clear').click(function(e) {
        e.preventDefault();
        context.clearRect(0, 0, j$('canvas').width(), j$('canvas').height());
    });
 
    j$('#save').click(function() {
        d = canvas.toDataURL('image/png');
        d = d.replace('image/png', 'image/octet-stream');
        window.open(d, 'save');
    });
    */

});

function finishAlert() {
    new Ajax.Request('save_micro.php',
    {
        method: 'POST',
        parameters:
        {
            src: d
        }
    });
    target1 = document.getElementById("greeting");
    target1.innerHTML = "<font color='red'>7秒が経過しました。次に遷移します。</font>";
    target2 = document.getElementById("button");
    target2.innerHTML = "";

    setTimeout("jumpPage()", 3000); // 3秒後にリロード
}

function jumpPage() {
    location.reload();
}

