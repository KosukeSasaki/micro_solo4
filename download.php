<?php

session_start();
// require_once('db_init.php');	// データベースの初期情報を読み込む
// require_once('const_init.php');   // 定数の定義
// require_once('function.php');   // 関数定義ファイルの読み込み

$file = $_SESSION['comp'];

if (file_exists($file)) {
    header('Content-type: image/png');
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename='.basename($file));
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
    exit;
}

?>
