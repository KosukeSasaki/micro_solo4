<?php

session_start();
// require_once('db_init.php');	// データベースの初期情報を読み込む
require_once('const_init.php');   // 定数の定義
require_once('function.php');   // 関数定義ファイルの読み込み

$_SESSION['execute'] = false;

?>

<!doctype html>
<html>
<head>
    <meta charset="UTF-8">
    <!-- <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/3.17.2/build/cssreset/cssreset-min.css"> -->
    <link rel="stylesheet" type="text/css" href="css/start.css">
    <!--
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">google.load("prototype", "1");</script>
    <script type="text/javascript">google.load("jquery", "1");</script>
    -->
    <script type="text/javascript" src="js/prototype.js"></script>
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/drawing_pr.js"></script>

</head>
<body>
<h1>実験にご協力いただき、ありがとうございます。</h1>

<h2>説明</h2>
本実験では、表示される写真の模写を行っていただきます。<br>
まずは、模写の完成例をご覧ください。
<img src="0.png"><br>
このような模写を目指してください。
<br><br>

続いて、模写の練習を行っていただきます。<br>
どのように線を引くことができるかをお確かめください。<br>

<div class="flame">
    <!-- <div id="pr"> -->
    <div class="wrapper" style="z-index:0">
    <span id="origin"><img class="target" src="1.jpg" style="opacity:0.5"></span>
    </div>

    <div class="wrapper" style="z-index:1">
            <canvas class="target" width="600" height="600">
                お使いのブラウザはHTML5のCanvas要素に対応していません。
            </canvas>
    </div>
    <!-- </div> -->
</div>
<b><u>模写としてこれ以上描く線がない</u></b>と判断した場合は、ページ下部の「これで完成!」ボタンを押してください。<br>
（このページには「これで完成!」ボタンはありません）<br>
模写の際は落ち着いて、ゆっくり正確に線を引いてください。<br><br>
説明は以上になります。<br>
説明が理解出来ましたら、下のリンクをクリックしてください。<br>
実験の本番が始まります。<br>
<a href="start.php">実験を開始する</a>

</body>

<?php
$cond_array = array();
$cond = 4;
for($i = 1; $i <= $cond; $i++){ // 1からDIR_MAXまでの数を配列に入れる
    array_push($cond_array, "$i");
}

shuffle($cond_array);
$_SESSION['cond_array'] = $cond_array;  // 配列の中身をシャッフルしてセッション変数に挿入
$_SESSION['iterator'] = 0;  // 試行回数をカウント
$_SESSION['cond'] = $cond;  // 実験条件数を保存

$_SESSION['displaySamePicture'] = false;

$solo_dir3 = "picture/3/";
$solo_dir4 = "picture/4/";
$countDir3 = countDir($solo_dir3);
$countDir4 = countDir($solo_dir4);
$individualDir = min($countDir3, $countDir4);
$_SESSION['indDir'] = $individualDir + 1;

?>

</html>

